﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Type{ union, intersection}
public class CreateVoxel : MonoBehaviour {

    // Use this for initialization
    public int subdivisions;
    public float rayon1,rayon2;
    public Vector3 centre1,centre2;
    public Type type = Type.union;
	void Start () {

        int rayon = (int)Mathf.Ceil(rayon1 + rayon2 + Vector3.Distance(centre1, centre2));
        for (int i = -rayon; i < rayon; i++)
        {
            for (int j = -rayon; j < rayon; j++)
            {
                for (int k = -rayon; k < rayon; k++)
                {
                    Vector3 cubeCenter = new Vector3(i, j, k);


                    if (type == Type.union) { 
                        if (Vector3.Distance(cubeCenter, centre1) < Vector3.Distance(cubeCenter, centre2))
                        {
                        
                            if (Vector3.Distance(cubeCenter, centre1) <= rayon1) { 
                                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                                cube.transform.position = new Vector3(i, j, k);
                            }
                        }
                        else
                        {
                            Debug.Log("got here");

                            if (Vector3.Distance(cubeCenter, centre2) <= rayon2)
                            {
                                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                                cube.transform.position = new Vector3(i, j, k);
                                
                            }
                        }
                    }
                    if(type == Type.intersection)
                    {
                        if(Vector3.Distance(cubeCenter, centre1) <= rayon1 && Vector3.Distance(cubeCenter, centre2) <= rayon2)
                        {
                            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                            cube.transform.position = new Vector3(i, j, k);
                        }
                    }

                }
            }
        }
    }


// Update is called once per frame
    void Update() {

      }
}
