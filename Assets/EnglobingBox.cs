﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnglobingBox : MonoBehaviour {

    // Use this for initialization

    public List<Vector3> spherePositions;
    public List<float> sphereRays;
    void Start () {
        int i = 0;
		foreach(Vector3 v in spherePositions){
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.position = v;
            sphere.transform.localScale = new Vector3(sphereRays[i], sphereRays[i], sphereRays[i]);
            i++;
        }
	}

    void englobingBox()
    {

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
